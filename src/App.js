import Navbar from "./components/Navbar";
import Sidebar from "./components/Sidebar";
import Home from "./components/Home";
import Footer from "./components/Footer";
import { Routes, Route } from "react-router-dom";
import About from "./components/About";
import Projects from "./components/Projects";
import Contact from "./components/Contact";
import Error from "./components/Error";
import "./App.css";
import { useEffect } from "react";
function App() {
  useEffect(() => {
    const navbar = document.querySelector("#nav");
    const date = document.querySelector("#date");
    // add fixed class to navbar
    window.addEventListener("scroll", function () {
      if (window.pageYOffset > 80) {
        navbar.classList.add("navbar-fixed");
      } else {
        navbar.classList.remove("navbar-fixed");
      }
    });
    // set year
    date.innerHTML = new Date().getFullYear();
  }, []);
  return (
    <div className="App">
      <Sidebar />
      <Routes>
        <Route path="/" element={<Navbar />}>
          <Route index element={<Home />} />
          <Route exact path="/about" element={<About />} />
          <Route exact path="/contact" element={<Contact />} />
          <Route exact path="/projects" element={<Projects />} />
          {/* <Route path="/user/:fname/:lname" element={<User />} /> */}
          <Route path="*" element={<Error />} />
        </Route>
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
