import React from "react";
import { NavLink, Outlet } from "react-router-dom";
import { FaBars } from "react-icons/fa";
const Navbar = () => {
  const handleClick = () => {
    const sidebar = document.querySelector("#sidebar");
    sidebar.classList.add("show-sidebar");
  };
  return (
    <div>
      <nav className="nav" id="nav">
        <div className="nav-center">
          <div className="nav-header">
            <NavLink to="/">
              <img
                src="./images/logo-no-background.svg"
                className="nav-logo"
                alt="nav logo"
              />
            </NavLink>
            <button className="nav-btn" id="nav-btn" onClick={handleClick}>
              <FaBars />
            </button>
          </div>
          <ul className="nav-links">
            <li>
              <NavLink to="/">home</NavLink>
            </li>
            <li>
              <NavLink to="/about">about</NavLink>
            </li>
            <li>
              <NavLink to="/projects">projects</NavLink>
            </li>
            <li>
              <NavLink to="/contact">contact</NavLink>
            </li>
          </ul>
        </div>
      </nav>
      <Outlet />
    </div>
  );
};

export default Navbar;
