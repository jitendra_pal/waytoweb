import React from "react";

const Timeline = () => {
  return (
    <section className="section timeline">
      <div className="section-title">
        <h2>timeline</h2>
        <div className="underline"></div>
      </div>
      <div className="section-center timeline-center">
        <article className="timeline-item">
          <h4>2023</h4>
          <p>
            Currently I'm Working in Wipro Limited as a senior software engineer
            and I have close to 6 years of work experience in IT industry.
          </p>
          <span className="number">1</span>
        </article>
        <article className="timeline-item">
          <h4>2020</h4>
          <p>
            Worked as software engineer in IT industry & Complted multiple
            projects in JavaScript, React Js, Microfrontends, Redux, HTML5,
            Wordpress, CSS3, Shopify, PHP & Figma
          </p>
          <span className="number">2</span>
        </article>
        <article className="timeline-item">
          <h4>2016</h4>
          <p>
            Completed training and certification in JavaScript (Frameworks) &
            PHP (MySql)
          </p>
          <span className="number">3</span>
        </article>
      </div>
    </section>
  );
};

export default Timeline;
