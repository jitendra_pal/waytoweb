import React from "react";

const Blog = () => {
  return (
    <section className="section blog">
      <div className="section-title">
        <h2>blog</h2>
        <div className="underline"></div>
      </div>
      <div className="section-center blog-center">
        <div className="card">
          <div className="card-side card-front">
            <img src="./images/project-1.jpeg" alt="" />
            <div className="card-info">
              <h4>article title</h4>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Et
                nisi ut a est eum tempora dolorum temporibus voluptatibus!
                Natus, provident.
              </p>
              <div className="card-footer">
                <img src="./images/hero-img-small.jpg" alt="author image" />
                <p>7 min read</p>
              </div>
            </div>
          </div>
          <div className="card-side card-back">
            <button className="btn">read more</button>
          </div>
        </div>
        <div className="card">
          <div className="card-side card-front">
            <img src="./images/project-2.jpeg" alt="" />
            <div className="card-info">
              <h4>article title</h4>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Et
                nisi ut a est eum tempora dolorum temporibus voluptatibus!
                Natus, provident.
              </p>
              <div className="card-footer">
                <img src="./images/hero-img-small.jpg" alt="author image" />
                <p>7 min read</p>
              </div>
            </div>
          </div>
          <div className="card-side card-back">
            <button className="btn">read more</button>
          </div>
        </div>
        <div className="card">
          <div className="card-side card-front">
            <img src="./images/project-3.jpeg" alt="" />
            <div className="card-info">
              <h4>article title</h4>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Et
                nisi ut a est eum tempora dolorum temporibus voluptatibus!
                Natus, provident.
              </p>
              <div className="card-footer">
                <img src="./images/hero-img-small.jpg" alt="author image" />
                <p>7 min read</p>
              </div>
            </div>
          </div>
          <div className="card-side card-back">
            <button className="btn">read more</button>
          </div>
        </div>
        <div className="card">
          <div className="card-side card-front">
            <img src="./images/project-4.jpeg" alt="" />
            <div className="card-info">
              <h4>article title</h4>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Et
                nisi ut a est eum tempora dolorum temporibus voluptatibus!
                Natus, provident.
              </p>
              <div className="card-footer">
                <img src="./images/hero-img-small.jpg" alt="author image" />
                <p>7 min read</p>
              </div>
            </div>
          </div>
          <div className="card-side card-back">
            <button className="btn">read more</button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Blog;
