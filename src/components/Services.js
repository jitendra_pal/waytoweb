import React from "react";
import { FaCode, FaSketch, FaGlobe } from "react-icons/fa";
const Services = () => {
  return (
    <div>
      <section className="section bg-grey">
        <div className="section-title">
          <h2>services</h2>
          <div className="underline"></div>
        </div>
        <div className="services-center section-center">
          <article className="service">
            <FaCode className="service-icon" />
            <h4>UI development</h4>
            <div className="underline"></div>
            <p>
              I love to work with frontend Technologies. like JavaScript, React
              & Redux Js, Vue Js, Angular Js, HTML5, CSS3, jQuery
            </p>
          </article>
          <article className="service">
            <FaSketch className="service-icon" />
            <h4>web design</h4>
            <div className="underline"></div>
            <p>
              I am using tools for designing i.e Figma, Adobe Photoshop, Adobe
              Illustrator, Adobe XD, Logo Design, 2D Animation
            </p>
          </article>
          <article className="service">
            <FaGlobe className="service-icon" />
            <h4>Backend development</h4>
            <div className="underline"></div>
            <p>
              In backend development I use to work with the Node js, PHP,
              CakePhp & Python
            </p>
          </article>
        </div>
      </section>
    </div>
  );
};

export default Services;
