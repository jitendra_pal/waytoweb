import React from "react";
import {
  FaFacebook,
  FaTwitter,
  FaLinkedin,
  FaInstagram,
  FaDownload,
  FaEnvelope,
} from "react-icons/fa";
import { TypeAnimation } from "react-type-animation";
const Header = () => {
  return (
    <div>
      <header className="hero">
        <div className="section-center hero-center">
          <article className="hero-info">
            <div className="underline"></div>
            <h1>i'm Jitendra Pal</h1>
            <h4>
              <TypeAnimation
                sequence={[
                  "UI Developer",
                  1000,
                  "Backend Developer",
                  1000,
                  "React Js Developer",
                  1000,
                  "UI Designer",
                  1000,
                  "Full Stack Developer",
                  1000,
                ]}
                speed={40} // Custom Speed from 1-99 - Default Speed: 40
                wrapper="span" // Animation will be rendered as a <span>
                repeat={Infinity} // Repeat this Animation Sequence infinitely
              />
            </h4>
            <a to="/contact" className="btn hero-btn">
              <FaEnvelope
                style={{ verticalAlign: "middle", marginRight: "5px" }}
              />
              <span>hire me</span>
            </a>
            <a
              href="resume/Jitendra_resume.pdf"
              className="btn hero-btn"
              style={{ marginLeft: "10px" }}
              download="resume/Jitendra_resume.pdf"
            >
              <FaDownload
                style={{ verticalAlign: "middle", marginRight: "5px" }}
              />
              <span>Resume</span>
            </a>
            <ul className="social-icons hero-icons">
              <li>
                <a
                  href="https://www.facebook.com/its.jitendrapal"
                  className="social-icon"
                  target="_blank"
                >
                  <FaFacebook />
                </a>
              </li>
              <li>
                <a
                  href="https://www.linkedin.com/in/zeetpaal/"
                  className="social-icon"
                  target="_blank"
                >
                  <FaLinkedin />
                </a>
              </li>
              <li>
                <a
                  href="https://www.twitter.com"
                  className="social-icon"
                  target="_blank"
                >
                  <FaTwitter />
                </a>
              </li>
              <li>
                <a
                  href="https://www.instagram.com/i.m.jitendrapal/"
                  className="social-icon"
                  target="_blank"
                >
                  <FaInstagram />
                </a>
              </li>
            </ul>
          </article>
          <article className="hero-img">
            <img
              src="./images/hero-img.png"
              className="hero-photo"
              alt="john doe"
            />
          </article>
        </div>
      </header>
    </div>
  );
};

export default Header;
