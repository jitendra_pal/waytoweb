import React from "react";
import Header from "./Header";
import About from "./About";
import Services from "./Services";
import Projects from "./Projects";
import Connect from "./Connect";
import Skills from "./Skills";
import Timeline from "./Timeline";
import Blog from "./Blog";
const Home = () => {
  return (
    <div>
      <Header />
      <About />
      <Services />
      <Projects />
      <Connect />
      <Skills />
      <Timeline />
      <Blog />
    </div>
  );
};

export default Home;
