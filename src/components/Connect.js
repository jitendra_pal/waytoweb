import React from "react";
import { NavLink } from "react-router-dom";
const Connect = () => {
  return (
    <section className="connect">
      <video
        controls
        autoplay
        muted
        loop
        className="video-container"
        poster="./images/project-1.jpeg"
      >
        <source src="./videos/connect.mp4" type="video/mp4" />
        Sorry, your browser does not support embedded videos
      </video>
      <div className="video-banner">
        <div className="section-title">
          <h2>let's get in touch</h2>
          <div className="underline"></div>
        </div>
        <p className="video-text">
          I’m familiar with a variety of programming languages, including
          JavaScript, React & Redux Js, Next Js, Node Js HTML, CSS, PHP,Laravel,
          jQuery, but I’m always adding new skills to my repertoire. I’m also
          eager to meet other software engineers in the area, so feel free to
          connect!
        </p>
        <NavLink to="/contact" className="btn">
          contact me
        </NavLink>
      </div>
    </section>
  );
};

export default Connect;
