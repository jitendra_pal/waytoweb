import React from "react";

const Contact = () => {
  return (
    <section class="section single-page">
      <div class="section-title">
        <h1>contact</h1>
        <div class="underline"></div>
      </div>
      <div class="section-center page-info">
        <p>
          If you are looking to get ahold of me, you can send me an email at
          <a href="mailto:itsjkp@gmail.com"> itsjkp@gmail.com</a>
        </p>
        <p>
          You can also reach me on Skype at
          <a href="#"> zeetpaal</a>
        </p>
      </div>
    </section>
  );
};

export default Contact;
