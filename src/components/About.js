import React from "react";
import { NavLink } from "react-router-dom";
const About = () => {
  return (
    <div>
      <section className="section about">
        <div className="section-center about-center">
          <article className="about-img">
            <img
              src="./images/about-img.jpg"
              className="hero-photo"
              alt="about img"
            />
          </article>
          <article className="about-info">
            <div className="section-title about-title">
              <h2>about</h2>
              <div className="underline"></div>
            </div>
            <p>I’m living the dream.</p>
            <p>
              I’ve always been a great problem solver, an independent introvert,
              and a technophile obsessed with the latest devices. Today, I’m
              working as a senior software engineer in Wipro limited, and I get
              to show off all these elements of who I am.
            </p>
            <p>
              I started learning to code when I was a teenager, though it was
              always more of a hobby than a career focus. After school
              education, I realized software engineering was the right field for
              me.
            </p>
            <NavLink to="/about" className="btn">
              about me
            </NavLink>
          </article>
        </div>
      </section>
    </div>
  );
};

export default About;
