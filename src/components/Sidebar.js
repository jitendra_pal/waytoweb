import React from "react";
import { NavLink } from "react-router-dom";
import {
  FaFacebook,
  FaTwitter,
  FaLinkedin,
  FaInstagram,
  FaTimes,
} from "react-icons/fa";
const Sidebar = () => {
  const handleClick = () => {
    const sidebar = document.querySelector("#sidebar");
    sidebar.classList.remove("show-sidebar");
  };
  return (
    <aside className="sidebar" id="sidebar">
      <div>
        <button className="close-btn" id="close-btn" onClick={handleClick}>
          <FaTimes />
        </button>
        <ul className="sidebar-links">
          <li>
            <NavLink to="/" onClick={handleClick}>
              home
            </NavLink>
          </li>
          <li>
            <NavLink to="/about" onClick={handleClick}>
              about
            </NavLink>
          </li>
          <li>
            <NavLink to="/projects" onClick={handleClick}>
              projects
            </NavLink>
          </li>
          <li>
            <NavLink to="/contact" onClick={handleClick}>
              contact
            </NavLink>
          </li>
        </ul>

        <ul className="social-icons">
          <li>
            <a
              href="https://www.facebook.com/its.jitendrapal"
              className="social-icon"
              target="_blank"
            >
              <FaFacebook />
            </a>
          </li>
          <li>
            <a
              href="https://www.linkedin.com/in/zeetpaal/"
              className="social-icon"
              target="_blank"
            >
              <FaLinkedin />
            </a>
          </li>
          <li>
            <a
              href="https://www.twitter.com"
              className="social-icon"
              target="_blank"
            >
              <FaTwitter />
            </a>
          </li>
          <li>
            <a
              href="https://www.instagram.com/i.m.jitendrapal/"
              className="social-icon"
              target="_blank"
            >
              <FaInstagram />
            </a>
          </li>
        </ul>
      </div>
    </aside>
  );
};

export default Sidebar;
