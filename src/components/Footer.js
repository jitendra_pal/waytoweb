import React from "react";
import { FaFacebook, FaTwitter, FaLinkedin, FaInstagram } from "react-icons/fa";
const Footer = () => {
  return (
    <footer class="footer">
      <ul class="social-icons">
        <li>
          <a
            href="https://www.facebook.com/its.jitendrapal"
            className="social-icon"
            target="_blank"
          >
            <FaFacebook />
          </a>
        </li>
        <li>
          <a
            href="https://www.linkedin.com/in/zeetpaal/"
            className="social-icon"
            target="_blank"
          >
            <FaLinkedin />
          </a>
        </li>
        <li>
          <a
            href="https://www.twitter.com"
            className="social-icon"
            target="_blank"
          >
            <FaTwitter />
          </a>
        </li>
        <li>
          <a
            href="https://www.instagram.com/i.m.jitendrapal/"
            className="social-icon"
            target="_blank"
          >
            <FaInstagram />
          </a>
        </li>
      </ul>

      <p>
        &copy; <span id="date"></span> Jitendra Pal. all rights reserved
      </p>
    </footer>
  );
};

export default Footer;
