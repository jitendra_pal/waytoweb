import React from "react";

const Projects = () => {
  return (
    <section className="section projects">
      <div className="section-title">
        <h2>latest works</h2>
        <div className="underline"></div>
        <p className="projects-text">
          I’m familiar with a variety of programming languages, including
          JavaScript, HTML, CSS, PHP,Laravel, jQuery, React Js and Redux, but
          I’m always adding new skills to my repertoire. Few of the project I
          have been added here
        </p>
      </div>
      <div className="section-center projects-center">
        <a href="projects.html" className="project-1">
          <article className="project">
            <img
              src="./images/project-1.jpeg"
              alt="single project"
              className="project-img"
            />
            <div className="project-info">
              <h4>project title</h4>
              <p>client name</p>
            </div>
          </article>
        </a>
        <a href="projects.html" className="project-2">
          <article className="project">
            <img
              src="./images/project-2.jpeg"
              alt="single project"
              className="project-img"
            />
            <div className="project-info">
              <h4>project title</h4>
              <p>client name</p>
            </div>
          </article>
        </a>
        <a href="projects.html" className="project-3">
          <article className="project">
            <img
              src="./images/project-3.jpeg"
              alt="single project"
              className="project-img"
            />
            <div className="project-info">
              <h4>project title</h4>
              <p>client name</p>
            </div>
          </article>
        </a>
        <a href="projects.html" className="project-4">
          <article className="project">
            <img
              src="./images/project-4.jpeg"
              alt="single project"
              className="project-img"
            />
            <div className="project-info">
              <h4>project title</h4>
              <p>client name</p>
            </div>
          </article>
        </a>
      </div>
    </section>
  );
};

export default Projects;
